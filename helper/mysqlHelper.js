var mysql = require('mysql');
var util = require('util');
var Promise = require('bluebird');

Promise.promisifyAll(mysql);
Promise.promisifyAll(require("mysql/lib/Connection").prototype);
Promise.promisifyAll(require("mysql/lib/Pool").prototype);

var Err = require('../error');

var poolOpt = {
    connectionLimit: 5,
    host: 'localhost',
    user: 'bittrader',
    password: process.env.MYSQL_PASSWD,
    supportBigNumbers: true,
    bigNumberStrings: true,
    database : 'bittrader',
    charset : 'utf8mb4'
}

var pool = mysql.createPool(poolOpt);

function escape(val) {
  return pool.escape.call(pool, val);
}


function getSqlConnection() {
  return pool.getConnectionAsync().disposer(function(connection) {
        connection.release();
  });
}

function query(queryString) {
  return Promise.using(getSqlConnection(), function(connection) {
    return connection.queryAsync(queryString);
  }).catch(function(e) {
    console.log(e);
    throw new Err.MySQLError(e);
  });
}

function withTransaction(fn) {
  return Promise.using(getSqlConnection(), function(connection) {
    var tx = connection.beginTransaction();
    return Promise
      .try(fn, tx)
      .then(
        function(res) {
          return connection.commitAsync().then(function(res) {
            return res;
          })
        },
      function(err) {
        console.log(err);
        return connection.rollbackAsync().catch(function(e) {
          throw new APIError.MySQLError(err);
        });
      });
  });
}

function end() {
  pool.end();
}

module.exports = {
  'query' : query,
  'withTransaction' : withTransaction,
  'escape' : escape,
  'end' : end
}