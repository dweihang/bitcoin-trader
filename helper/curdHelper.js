var mysqlClient = require('./mysqlHelper');
var squel = require('./squelExtension');

function save(dataObj, tableName) {
    var sqlQueryBuilder = squel
    .insert()
    .into(tableName)
    .setFieldsRows([
      dataObj
    ]);

    return mysqlClient.query(sqlQueryBuilder.toString());
}

function batchSave(dataObjArray, tableName) {
    var sqlQueryBuilder = squel
    .insert()
    .into(tableName)
    .setFieldsRows(dataObjArray);

    return mysqlClient.query(sqlQueryBuilder.toString());
}

module.exports = {
    'save' : save,
    'batchSave' : batchSave
}
