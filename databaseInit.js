var Promise = require('bluebird');
var mysqlClient = require('./helper/mysqlHelper');
var squel = require('./helper/squelExtension');
var CurrencyType = require('./const').CurrencyType;
var KLineType = require('./const').KLineType;
var MarketDepthType = require('./const').MarketDepthType;
var Const = require('./const');

function createHuoBiRealTimeKLineTable(kLineType, currencyType) {
    var sqlBuilder = squel.createIfNotExists()
    .table(Const.getKLineTableNameByType(kLineType, currencyType))
    .field('ts', 'decimal(15) NOT NULL')
    .field('id', 'varchar(50) NOT NULL')
    .field('amount', 'decimal(15,6) NOT NULL')
    .field('count', 'int NOT NULL')
    .field('open', 'decimal(8,2) NOT NULL')
    .field('close', 'decimal(8,2) NOT NULL')
    .field('low', 'decimal(8,2) NOT NULL')
    .field('high', 'decimal(8,2) NOT NULL')
    .field('vol', 'decimal(17,6) NOT NULL')

    return mysqlClient.query(sqlBuilder.toString());
}

function createHuoBiRealTimeMarketDepthTable(depthType, currencyType) {
    var sqlBuilder = squel.createIfNotExists()
    .table(Const.getMarketDepthTableNameByType(depthType, currencyType))
    .field('ts', 'decimal(15) NOT NULL')
    .field('price', 'decimal(8,2) NOT NULL')
    .field('amount', 'decimal(15,6) NOT NULL')
    .field('type', 'tinyint(1) NOT NULL')

    return mysqlClient.query(sqlBuilder.toString());
}

function createHuoBiRealTimeTradeDetailTable(currencyType) {
    var sqlBuilder = squel.createIfNotExists()
    .table(Const.getTradeDetailTableNameByCurrencyType(currencyType))
    .field('ts', 'decimal(15) NOT NULL')
    .field('price', 'decimal(8,2) NOT NULL')
    .field('time', 'int NOT NULL')
    .field('amount', 'decimal(15,6) NOT NULL')
    .field('direction', 'varchar(20) NOT NULL')
    .field('type', 'varchar(20) DEFAULT NULL')
    .field('tradeId', 'varchar(20) NOT NULL')
    .field('id', 'varchar(20) NOT NULL');

    return mysqlClient.query(sqlBuilder.toString());
}


function createHuoBiRealTimeMarketDetailTable(currencyType) {
    var sqlBuilder = squel.createIfNotExists()
    .table(Const.getMarketDetailTableNameByCurrencyType(currencyType))
    .field('ts_root', 'decimal(15) NOT NULL')
    .field('ts', 'decimal(15) NOT NULL')
    .field('amount', 'decimal(15,6) NOT NULL')
    .field('open', 'decimal(8,2) NOT NULL')
    .field('close', 'decimal(8,2) NOT NULL')
    .field('high', 'decimal(8,2) NOT NULL')
    .field('count', 'int NOT NULL')
    .field('low', 'decimal(8,2) NOT NULL')
    .field('vol', 'decimal(17,6) NOT NULL');

    return mysqlClient.query(sqlBuilder.toString());
}


function initDatabase() {
    var promises = [];
    for (var currencyType in CurrencyType) {
        if (CurrencyType.hasOwnProperty(currencyType)) {
            for (var kLineType in KLineType) {
                if (KLineType.hasOwnProperty(kLineType)) {
                    promises.push(createHuoBiRealTimeKLineTable(kLineType, currencyType));
                }
            }
            for (var marketDepth in MarketDepthType) {
                if (MarketDepthType.hasOwnProperty(marketDepth)) {
                    promises.push(createHuoBiRealTimeMarketDepthTable(marketDepth, currencyType));
                }
            }
            promises.push(createHuoBiRealTimeTradeDetailTable(currencyType));
            promises.push(createHuoBiRealTimeMarketDetailTable(currencyType));
        }
    }
    return Promise.all(promises);
}


function init() {
    return initDatabase()
    .then(function(){
        console.log('database init finished');
    });
}

module.exports = {'init' : init};

