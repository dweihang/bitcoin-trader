var Err = {};

Err.ErrorMsg = {};
Err.ErrorMsg.DEFAULT_PROGROM_ERROR = "程序好像出错了";
Err.ErrorMsg.DEFAULT_DB_ERROR = "数据库好像怠工了";

function ParamsError(message) {
  this.name = "ParamsError";
  this.message = message || Err.ErrorMsg.DEFAULT_PARAMS_ERROR;
}
ParamsError.prototype = new Error();

function DBError() {
  this.name = "DBError";
}
DBError.prototype = new Error();

function RedisError(obj) {
  this.name = "RedisError";
  this.errorObj = obj;
}
RedisError.prototype = new DBError();

function MySQLError(obj) {
  this.name = "MySQLError";
  this.errorObj = obj;
}
MySQLError.prototype = new DBError();

function ChatServerError(obj) {
  this.name = "ChatServerError";
  this.errorObj = obj;
}
ChatServerError.prototype = new Error();

Err.RedisError = RedisError;
Err.MySQLError = MySQLError;
Err.DBError = DBError;
Err.ParamsError = ParamsError;

module.exports = Err;
