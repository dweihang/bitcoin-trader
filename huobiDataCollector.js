var DataClient = require('./huobiDataClient');
var curdHelper = require('./helper/curdHelper')
var CurrencyType = require('./const').CurrencyType;
var KLineType = require('./const').KLineType;
var MarketDepthType = require('./const').MarketDepthType;
var Address = require('./const').Address;
var Const = require('./const');
const Subject = require('@reactivex/rxjs').Subject;

var DataCollector = (function() {
    var _kLineClients;
    var _marketDepthClients;
    var _tradeDetailClients;
    var _marketDetailClients;

    var _objectSaveEventNotifier = new Subject();

    function createClients() {
         _kLineClients = [];
         _marketDepthClients = [];
         _tradeDetailClients = [];
         _marketDetailClients = [];

        for (var currencyType in CurrencyType) {
            if (CurrencyType.hasOwnProperty(currencyType)) {
                for (var kLineType in KLineType) {
                    if (KLineType.hasOwnProperty(kLineType)) {
                        _kLineClients.push(DataClient(Address[currencyType], Const.getKLineChannelByType(kLineType, currencyType), {kLineType : kLineType, currencyType : currencyType}));
                    }
                }
                for (var marketDepth in MarketDepthType) {
                    if (MarketDepthType.hasOwnProperty(marketDepth)) {
                        // _marketDepthClients.push(DataClient(Address[currencyType], Const.getMarketDepthChannelByType(marketDepth, currencyType), {marketDepth : marketDepth, currencyType : currencyType}));
                    }
                }
                _tradeDetailClients.push(DataClient(Address[currencyType], Const.getTradeDetailChannelByCurrencyType(currencyType), {currencyType : currencyType}));
                _marketDetailClients.push(DataClient(Address[currencyType], Const.getMarketDetailChannelByCurrencyType(currencyType), {currencyType : currencyType}));
            }
        }
    }

    function DataCollector() {}

    DataCollector.prototype.startCollectData = function() {
        createClients();
        for (var i=0; i < _kLineClients.length; i++) {
            (function(i) {
                var props = _kLineClients[i].getExtraProperties();
                _kLineClients[i].data()
                .subscribe(function(dataObj) {
                    var savedObj = dataObj.tick;
                    savedObj['ts'] = dataObj.ts;
                    
                    curdHelper.save(savedObj, Const.getKLineTableNameByType(props['kLineType'], props['currencyType']))
                    .then(function(result){
                        _objectSaveEventNotifier.next({obj : savedObj});
                    })
                    .catch(function(e){
                        _objectSaveEventNotifier.next({error : e, obj : savedObj})
                    });
                });
            })(i);
        }
        for (var i=0; i < _marketDepthClients.length; i++) {
            (function(i) {
                var props = _marketDepthClients[i].getExtraProperties();
                _marketDepthClients[i].data()
                .subscribe(function(dataObj) {
                    var savedObjArr = [];
                    for (var i=0, len=dataObj.tick.asks.length; i < len; i++) {
                        var savedObj = {'ts' : dataObj.ts};
                        savedObj['type'] = 0;
                        savedObj['price'] = dataObj.tick.asks[i][0];
                        savedObj['amount'] = dataObj.tick.asks[i][1];
                        savedObjArr.push(savedObj);
                    }
                    for (var i=0, len=dataObj.tick.bids.length; i < len; i++) {
                        var savedObj = {'ts' : dataObj.ts};
                        savedObj['type'] = 1;
                        savedObj['price'] = dataObj.tick.bids[i][0];
                        savedObj['amount'] = dataObj.tick.bids[i][1];
                        savedObjArr.push(savedObj);
                    }
                    curdHelper.batchSave(savedObjArr, Const.getMarketDepthTableNameByType(props['marketDepth'], props['currencyType']))
                    .then(function(result){
                        _objectSaveEventNotifier.next({obj : savedObjArr});
                    })
                    .catch(function(e){
                        _objectSaveEventNotifier.next({error : e, obj : savedObjArr});
                    });
                });
            })(i);
        }
        for (var i=0; i < _marketDetailClients.length; i++) {
            (function(i) {
                var props = _marketDetailClients[i].getExtraProperties();
                _marketDetailClients[i].data()
                .subscribe(function(dataObj) {
                    var savedObj = dataObj.tick;
                    delete savedObj['id'];
                    savedObj['ts_root'] = dataObj.ts;
                    curdHelper.save(savedObj, Const.getMarketDetailTableNameByCurrencyType(props['currencyType']))
                    .then(function(result) {
                        _objectSaveEventNotifier.next({obj : savedObj});
                    })
                    .catch(function(e) {
                        _objectSaveEventNotifier.next({error : e, obj : savedObj});
                    });
                });
            })(i);
        }
        for (var i=0; i < _tradeDetailClients.length; i++) {
            (function(i) {
                var props = _tradeDetailClients[i].getExtraProperties();
                _tradeDetailClients[i].data()
                .subscribe(function(dataObj) {
                    var savedObjArr = dataObj.tick.data;
                    savedObjArr.forEach(function(ele){
                        ele['id'] = ele['id'] + '';
                        ele['tradeId'] = ele['tradeId'] + '';
                    });
                    curdHelper.batchSave(savedObjArr, Const.getTradeDetailTableNameByCurrencyType(props['currencyType']))
                    .then(function(result) {
                        _objectSaveEventNotifier.next({obj : savedObjArr});
                    })
                    .catch(function(e) {
                        _objectSaveEventNotifier.next({error : e, obj : savedObjArr});
                    });
                });
            })(i);
        }
        return _objectSaveEventNotifier;
    }
	
	return DataCollector;

})();

module.exports = DataCollector;
