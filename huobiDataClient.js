const ws = require('ws');
const pako = require('pako');
const Observable = require('@reactivex/rxjs').Observable;
const Subject = require('@reactivex/rxjs').Subject;

const Status = {
    'Idle' : 0,
    'Connecting' : 1,
    'Connected' : 2,
    'Disconnecting' : 3,
    'Disconnected' : 4
}

function DataClient(url, channel, extraProps){
     if (!url) {
            throw "url cannot be null";
    }
    if (!channel) {
        throw  "channel cannot be null";
    }

    var _dataNotifier;
    var _connectionNotifier;

    var _destUrl = url;
    var _channel = channel;
    var _extraProps = extraProps;

    var _msgCallback;

    var _clientStatus = Status.Idle;
    var _wsClient;

    function inflate(buffer) {
        return pako.inflate(buffer, { to: 'string' });
    }

    function deflate(str) {
        return pako.deflate(str);
    }

    function inflateJSON(buffer) {
        return JSON.parse(inflate(buffer));
    }

    function deflateJSON(json) {
        return deflate(JSON.stringify(json));
    }

    function isHeartBeatMessageObject(dataObj) {
        if (dataObj.ping != undefined && dataObj.ping != null) {
            return true;
        }
        return false;
    }

    function isSubscribedChannelObject(dataObj) {
        if (dataObj.subbed != undefined && dataObj.subbed != null) {
            return true;
        }
        return false;
    }

    function handleHeartBeat(dataObj) {
        if (dataObj.ping != undefined && dataObj.ping != null) {
            var pongObj = {
            'pong' : dataObj.ping
            }
            _wsClient.send(JSON.stringify(pongObj));
            return true;
        }
        return false;
    }

    function connect() {
        if (_clientStatus == Status.Connecting) {
            return;
        }
        _clientStatus = Status.Connecting;

        if (!_connectionNotifier) {
            _connectionNotifier = new Subject();
        }
        _connectionNotifier.next(_clientStatus);

        if (_wsClient) {
            if (_msgCallback) {
                _wsClient.removeListener('message', _msgCallback);
            }
           _wsClient.terminate();
        }

        _wsClient = new ws(_destUrl);
        _wsClient.on('open', function() {
            _clientStatus = Status.Connected;
            _connectionNotifier.next(_clientStatus);
        });
            
        _wsClient.on('close', function() {
            _clientStatus = Status.Disconnected;
            _connectionNotifier.next(_clientStatus);
            connect();
        });

        _wsClient.on('message', function(data) {
            var dataObj = inflateJSON(data);
            handleHeartBeat(dataObj);
        });

        return _connectionNotifier;
    }

    function subscribeChannel(channel) {
        var connectionObservable;

        if (_clientStatus == Status.Connected) {
            connectionObservable = Observable.of(_clientStatus);
        } else if (_clientStatus == Status.Connecting) {
            connectionObservable = _connectionNotifier
            .filter(function(status) {
                return status == Status.Connected;
            });
        } else {
            connectionObservable = connect()
            .filter(function(status) {
                return status == Status.Connected;
            });
        }
        
        
        return connectionObservable.mergeMap(function() {
            return Observable.create(function(observer) {
                _wsClient.send(JSON.stringify({
                    'sub': channel
                }));

                var listener = function(data) {
                    var dataObj = inflateJSON(data);
                    if (dataObj && dataObj.subbed == channel) {
                        observer.next(channel);
                        // _wsClient.removeListener('message', listener);
                    }
                }

                _wsClient.on('message', listener);
            });
        });
    }

    function DataClient() {}

    DataClient.Status = JSON.parse(JSON.stringify(Status));

    DataClient.prototype.getUrl = function() {
        return _destUrl;
    }

    DataClient.prototype.getChannel = function() {
        return _channel;
    }

    DataClient.prototype.getExtraProperties = function() {
        return _extraProps;
    }

    DataClient.prototype.connectionStatus = function() {
        if (!_connectionNotifier) {
            _connectionNotifier = new Subject();
        }
        return _connectionNotifier;
    }

    DataClient.prototype.terminate = function() {
        if (_wsClient) {
            _wsClient.terminate();
            _wsClient = null;
        }
        _clientStatus = Status.Idle;
        if (_connectionNotifier) {
            _connectionNotifier.next(_clientStatus);
        }
    }

    DataClient.prototype.data = function() {
        return subscribeChannel(_channel)
        .mergeMap(function() {
            if (!_dataNotifier) {
                _dataNotifier = new Subject();
            }
            if (!_msgCallback) {
                _msgCallback = function (msg) {
                    var dataObj = inflateJSON(msg);
                    if (!isHeartBeatMessageObject(dataObj) && !isSubscribedChannelObject(dataObj)) {
                        _dataNotifier.next(dataObj);
                    }
                }
                _wsClient.on('message', _msgCallback);
            }
            return _dataNotifier;
        });
    }

    return new DataClient();
}

module.exports = DataClient;
