var KLineType = {
    '1min' : '1min', 
    '5min' : '5min', 
    '15min' : '15min', 
    '30min' : '30min', 
    '60min' : '60min', 
    '1day' : '1day',
    '1mon' : '1mon',
    '1week' : '1week'
}

var CurrencyType = {
    'btccny' : 'btccny',
    'ethcny' : 'ethcny',
    'ltccny' : 'ltccny',
    'etccny' : 'etccny'
}

var Address = {
    'btccny' : 'wss://api.huobi.com/ws',
    'ltccny' : 'wss://api.huobi.com/ws',
    'ethcny' : 'wss://be.huobi.com/ws',
    'etccny' : 'wss://be.huobi.com/ws'
}

var MarketDepthType = {
    'step0' : 'step0',
    'step1' : 'step1',
    'step2' : 'step2',
    'step3' : 'step3',
    'step4' : 'step4',
    'step5' : 'step5'
}

function getTradeDetailTableNameByCurrencyType(currencyType) {
    return "huobi_" + currencyType + "_rt_trade_detail_tbl";
}

function getTradeDetailChannelByCurrencyType(currencyType) {
    return "market." + currencyType + ".trade.detail"
}

function getMarketDepthTableNameByType(type, currencyType) {
    return "huobi_" + currencyType + "_rt_depth_" + type + "_tbl"
}

function getMarketDepthChannelByType(type, currencyType) {
    return "market." + currencyType + ".depth." + type;
}

function getKLineTableNameByType(kLineType, currencyType) {
    return "huobi_" + currencyType + "_rt_kline_" + kLineType + "_tbl";
}

function getKLineChannelByType(kLineType, currencyType) {
    return "market." + currencyType + ".kline." + kLineType;
}

function getMarketDetailTableNameByCurrencyType(currencyType) {
    return "huobi_" + currencyType + "_rt_market_detail_tbl";
}

function getMarketDetailChannelByCurrencyType(currencyType) {
    return "market." + currencyType + ".detail"; 
}

module.exports = {
    KLineType : KLineType,
    CurrencyType : CurrencyType,
    MarketDepthType : MarketDepthType,
    Address : Address,

    getTradeDetailTableNameByCurrencyType : getTradeDetailTableNameByCurrencyType,
    getTradeDetailChannelByCurrencyType : getTradeDetailChannelByCurrencyType,
    getMarketDepthTableNameByType : getMarketDepthTableNameByType,
    getMarketDepthChannelByType : getMarketDepthChannelByType,
    getKLineTableNameByType : getKLineTableNameByType,
    getKLineChannelByType : getKLineChannelByType,
    getMarketDetailTableNameByCurrencyType : getMarketDetailTableNameByCurrencyType,
    getMarketDetailChannelByCurrencyType : getMarketDetailChannelByCurrencyType
}